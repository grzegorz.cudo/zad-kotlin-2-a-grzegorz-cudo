fun main() {
    val lekcja = 1

    val przedmiot = when (lekcja) {
        1 -> "Podstawy Programowania"
        2 -> "Matematyka"
        3 -> "Jezyk polski"
        4 -> "Jezyk angielski"
        5 -> "Jezyk niemiecki"
        6 -> "Biologia"
        7 -> "Geografia"
        8 -> "Religia"
        9 -> "Historia"
        10 -> "Chemia"
        11 -> "Fizyka"
        12 -> "Podstawy Przedsiebiorczosci"
        else -> "Nieprawidłowy numer"
    }

    println(przedmiot)
}
